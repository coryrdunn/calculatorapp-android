package cdunn.calculator;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class ButtonFrag extends Fragment {


    public ButtonFrag() {
        // Required empty public constructor
    }

    private View.OnClickListener btnNumListener = new View.OnClickListener() {
        public void onClick(View v) {
            Button btnV = (Button) v;
            String value = (String) btnV.getText();

            MainActivity ma = (MainActivity) getActivity();
            ma.displayValue(value);
        }
    };

    private View.OnClickListener btnOperatorListener = new View.OnClickListener() {
        public void onClick(View v) {
            Button btnV = (Button) v;
            String value = (String) btnV.getText();

            MainActivity ma = (MainActivity) getActivity();
            ma.setOperator(value);
        }
    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_button, container, false);

        //Wire-Up Number Buttons
        Button btnZero = (Button) rootview.findViewById(R.id.btnZero);
        Button btnOne = (Button) rootview.findViewById(R.id.btnOne);
        Button btnTwo = (Button) rootview.findViewById(R.id.btnTwo);
        Button btnThree = (Button) rootview.findViewById(R.id.btnThree);
        Button btnFour = (Button) rootview.findViewById(R.id.btnFour);
        Button btnFive = (Button) rootview.findViewById(R.id.btnFive);
        Button btnSix = (Button) rootview.findViewById(R.id.btnSix);
        Button btnSeven = (Button) rootview.findViewById(R.id.btnSeven);
        Button btnEight = (Button) rootview.findViewById(R.id.btnEight);
        Button btnNine = (Button) rootview.findViewById(R.id.btnNine);

        //Wire-Up Operation Buttons
        Button btnAdd = (Button) rootview.findViewById(R.id.btnAdd);
        Button btnSub = (Button) rootview.findViewById(R.id.btnSub);
        Button btnMultiply = (Button) rootview.findViewById(R.id.btnMultiply);
        Button btnDiv = (Button) rootview.findViewById(R.id.btnDiv);
        Button btnDot = (Button) rootview.findViewById(R.id.btnDot);
        Button btnEquals = (Button) rootview.findViewById(R.id.btnEquals);
        Button btnCLR = (Button) rootview.findViewById(R.id.btnCLR);

        //Set-Up Number Listeners
        btnZero.setOnClickListener(btnNumListener);
        btnOne.setOnClickListener(btnNumListener);
        btnTwo.setOnClickListener(btnNumListener);
        btnThree.setOnClickListener(btnNumListener);
        btnFour.setOnClickListener(btnNumListener);
        btnFive.setOnClickListener(btnNumListener);
        btnSix.setOnClickListener(btnNumListener);
        btnSeven.setOnClickListener(btnNumListener);
        btnEight.setOnClickListener(btnNumListener);
        btnNine.setOnClickListener(btnNumListener);
        btnDot.setOnClickListener(btnNumListener);

        //Set-Up Operator Listeners
        btnAdd.setOnClickListener(btnOperatorListener);
        btnSub.setOnClickListener(btnOperatorListener);
        btnMultiply.setOnClickListener(btnOperatorListener);
        btnDiv.setOnClickListener(btnOperatorListener);

        //Set-Up Equals Listener
        btnEquals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity ma = (MainActivity) getActivity();
                ma.evaluate();
            }
        });

        //Set-Up Clear Listener
        btnCLR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity ma = (MainActivity) getActivity();
                ma.clear();
            }
        });



        return rootview;
    }

}
