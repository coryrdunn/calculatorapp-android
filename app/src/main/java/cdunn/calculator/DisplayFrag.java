package cdunn.calculator;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class DisplayFrag extends Fragment {

    //VARIABLES
    TextView txvDisplay;
    String operator;
    String s;
    float valueOne = 0;
    float valueTwo = 0;
    float result;


    public DisplayFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_display, container, false);

        txvDisplay = (TextView) rootview.findViewById(R.id.txvDisplay);

        return rootview;
    }

    public void displayValue(String value) {

        //Clear the display after operator has been selected
        if(valueTwo == 0 && operator != null){

            s = null;
            txvDisplay.setText(null);

        }

        //Concatenate multiple numbers together
        if(s == null){
            s = value;
        }
        else {
            s = s.concat(value);
        }

        //Display number to screen
        txvDisplay.setText(s);

        //Set values to be calculated
        if(operator == null){
            valueOne = Float.parseFloat(s);
        }
        else {
            valueTwo = Float.parseFloat(s);
        }

    }

    //Set the desired operation
    public void setOperator(String value){

        //Allow multiple operations to be performed without hitting equals
        if(operator != null) {
            evaluate();
            valueOne = result;
            valueTwo = 0;
        }

        operator = value;

    }

    //Determine Result and Display to Screen
    public void evaluate() {

        switch(operator) {
            case "+":
                result = valueOne + valueTwo;
                break;
            case "-":
                result = valueOne - valueTwo;
                break;
            case "x":
                result = valueOne * valueTwo;
                break;
            case "/":
                result = valueOne / valueTwo;
                break;
            default:
                result = 0;
                break;
        }

        txvDisplay.setText(Float.toString(result));
    }

    //Clear everything
    public void clear() {
        txvDisplay.setText(null);
        valueOne = 0;
        valueTwo = 0;
        operator = null;
        result = 0;
        s = null;
    }

    @Override
    public void onResume() {
        super.onResume();

        SharedPreferences prefs = getActivity().getPreferences(getActivity().MODE_PRIVATE);
        txvDisplay.setText(prefs.getString("txvDisplay", null));
        operator = prefs.getString("operator", null);
        valueOne = prefs.getFloat("valueOne", 0);
        valueTwo = prefs.getFloat("valueTwo", 0);
        s = prefs.getString("s", null);
        result = prefs.getFloat("result", 0);

    }

    @Override
    public void onPause() {
        super.onPause();

        SharedPreferences prefs = getActivity().getPreferences(getActivity().MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("txvDisplay", txvDisplay.getText().toString());
        editor.putString("operator", operator);
        editor.putFloat("valueOne", valueOne);
        editor.putFloat("valueTwo", valueTwo);
        editor.putString("s", s);
        editor.putFloat("result", result);
        editor.apply();

    }
}
