package cdunn.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.ButtonFrag, new ButtonFrag(), "BF")
                .add(R.id.DisplayFrag, new DisplayFrag(), "DF")
                .commit();

    }

    @Override
    protected void onPause() {
        super.onPause();

        getSupportFragmentManager().beginTransaction()
                .remove(getSupportFragmentManager().findFragmentByTag("DF"))
                .remove(getSupportFragmentManager().findFragmentByTag("BF"))
                .commit();
    }

    public void displayValue(String value) {
        DisplayFrag d = (DisplayFrag) getSupportFragmentManager().findFragmentByTag("DF");
        d.displayValue(value);
    }

    public void setOperator(String value) {
        DisplayFrag d = (DisplayFrag) getSupportFragmentManager().findFragmentByTag("DF");
        d.setOperator(value);
    }

    public void evaluate(){
        DisplayFrag d = (DisplayFrag) getSupportFragmentManager().findFragmentByTag("DF");
        d.evaluate();
    }

    public void clear() {
        DisplayFrag d = (DisplayFrag) getSupportFragmentManager().findFragmentByTag("DF");
        d.clear();
    }
}
